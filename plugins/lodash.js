import _ from 'lodash'

export default (ctx, inject) => {
  inject('_', _)
  inject('lodash', _)
}
