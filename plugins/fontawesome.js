import Vue from 'vue'
import { library, config } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import {
  faAngleLeft,
  faAngleRight,
  faCommentDots,
  faPencilAlt,
  faSearch
} from '@fortawesome/free-solid-svg-icons'

config.autoAddCss = false

library.add(faAngleLeft, faAngleRight, faCommentDots, faPencilAlt, faSearch)

Vue.component('fa', FontAwesomeIcon)
