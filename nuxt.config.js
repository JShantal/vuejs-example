import moment from 'moment'
import pkg from './package'
import newsRoutes from './routes/news'

export default {
  mode: 'universal',
  head: {
    title: 'mvp.loc',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    script: [
      {
        src:
          'https://script.days.ru/calendar.php?advanced=1&hrams=0&hram=0&name=0&trop=0&icon=0&tmshift=' +
          (-4 - moment().utcOffset() / 60)
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  loading: { color: '#489' },
  css: [
    { src: '@/assets/scss/app.scss', lang: 'scss' },
    { src: '@fortawesome/fontawesome-svg-core/styles.css' }
  ],
  plugins: [
    { src: '~/plugins/line-clamp.js', ssr: false },
    { src: '@/plugins/fontawesome.js' },
    { src: '@/plugins/lodash.js' }
  ],
  modules: [
    '@nuxtjs/axios',
    ['bootstrap-vue/nuxt', { css: false }],
    ['@nuxtjs/moment', { locales: ['ru'], defaultLocale: 'ru' }],
    '@nuxtjs/pwa'
  ],
  router: {
    linkActiveClass: 'active',
    linkExactActiveClass: 'exact-active',
    extendRoutes(routes) {
      routes.push(newsRoutes)
    }
  },
  axios: {
    debug: true
  },
  build: {
    /*
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
     */
  }
}
