import _ from 'lodash'
import moment from 'moment'

function min(values) {
  values = _.map(values, _.parseInt)
  return _.min(values)
}

function fixZero(value) {
  if (value < 10) {
    return '0' + value
  }
  return value
}

function handleDateValue(value, field, ctx) {
  value = value[field]
  const upperField = _.upperFirst(field)
  const max = ctx.getters['max' + upperField]
  let min = 1
  if (field === 'month') {
    value = value - 1
  }
  if (field === 'day') {
    field = 'date'
  } else {
    min = ctx.getters['min' + upperField]
  }
  value = _.parseInt(value)
  if (isNaN(value)) {
    value = moment()[field]()
  } else if (value > max) {
    value = max
  } else if (value < min) {
    value = min
  }

  return value
}

export const state = () => ({
  categories: [
    {
      name: 'Категория',
      slug: 'category'
    },
    {
      name: 'Категория2',
      slug: 'category2'
    }
  ],
  url: 'news',
  year: moment().year(),
  month: moment().month(),
  day: moment().date(),
  dates: {
    2018: {
      1: [1, 2, 6],
      3: [7, 8, 9],
      4: [8, 9, 16],
      5: [5, 20],
      6: [6],
      7: [7],
      8: [8],
      9: [9],
      10: [10],
      11: [11]
    }
  }
})

export const mutations = {
  setUrl(state, value) {
    let url = 'news'
    if (value.tag) {
      url += '/tag/' + value.tag
    } else if (value.category) {
      url += '/category/' + value.category
    }
    state.url = url
  },
  setYear(state, value) {
    state.year = value
  },
  setMonth(state, value) {
    state.month = value
  },
  setDay(state, value) {
    state.day = value
  }
}

export const getters = {
  minYear: state => {
    const years = Object.keys(state.dates)
    return min(years)
  },
  maxYear: () => {
    return moment().year()
  },
  minMonth: (state, getters) => (year = state.year) => {
    const minYear = getters.minYear
    if (minYear === year) {
      const months = Object.keys(state.dates[minYear])
      return min(months)
    }
    return 0
  },
  maxMonth: (state, getters) => (year = state.year) => {
    if (getters.maxYear === year) {
      return moment().month()
    }
    return 11
  },
  maxDay: state => {
    return moment(state.year + '-' + (state.month + 1), 'YYYY-MM').daysInMonth()
  },
  dateLink: state => (year, month = null, date = null) => {
    let link
    if (state.dates && state.dates[year]) {
      link = '/' + state.url + '/' + year
    } else {
      return
    }
    if (month === null) {
      return link
    } else if (state.dates[year][month]) {
      link += '/' + fixZero(month + 1)
    } else {
      return
    }
    if (date === null) {
      return link
    } else if (_.includes(state.dates[year][month], date)) {
      return (link += '/' + fixZero(date))
    }
    return false
  }
}

export const actions = {
  setDate(ctx, value) {
    ctx.commit('setYear', handleDateValue(value, 'year', ctx))
    ctx.commit('setMonth', handleDateValue(value, 'month', ctx))
    ctx.commit('setDay', handleDateValue(value, 'day', ctx))
  }
}
