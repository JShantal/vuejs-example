const component = '~/components/news/page.vue'

function generateDateRoutes() {
  return {
    path: ':year',
    component,
    // name: 'index',
    children: [
      {
        path: ':month',
        component,
        children: [{ path: ':day', component }]
      }
    ]
  }
}

export default {
  path: '/news',
  name: 'news',
  component,
  children: [
    {
      path: 'category/:category',
      name: 'news-category',
      component,
      children: [generateDateRoutes()]
    },
    {
      path: 'tag/:tag',
      name: 'news-tag',
      component,
      children: [generateDateRoutes()]
    },
    generateDateRoutes()
  ]
}
